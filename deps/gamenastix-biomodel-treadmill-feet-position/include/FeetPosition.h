#pragma warning(disable : 4996)
#define GLM_ENABLE_EXPERIMENTAL
#include "deepModel/Treadmill.h"
#include "glm/glm.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include <Protocol.h>
#include <algorithm>
#include <openvr_driver.h>


#define YAW_INDEX 0
#define PITCH_INDEX 1
#define ROLL_INDEX 2
#define MAX_XBOX_STICK (32767)
#define MAX_XBOX_TRIGGER (255)

namespace FeetPosition
{
	struct TrackersPosition
	{
	    glm::vec3 leftFootPosition;
	    glm::vec3 rightFootPosition;
	};

	glm::vec3 getRotated(glm::vec3 normal, float pitchDegrees, float rollDegrees)
	{
	    auto afterRoll = glm::rotate(normal, glm::radians(rollDegrees), glm::vec3{0, 0, 1});
	    auto afterPitch = glm::rotate(afterRoll, -glm::radians(pitchDegrees), glm::vec3{1, 0, 0});
	#ifdef VERBOSE
	    std::cout << normal << " " << afterRoll << " " << afterPitch << std::endl;
	#endif
	    return afterPitch;
	}
	
	glm::vec3 getFootPosition(float yaw, glm::vec3 offset, glm::vec3 yawPitchRollUp, glm::vec3 yawPitchRollBottom)
	{
	    glm::vec3 pelvisToKneeNPose = {0, -0.5, 0};
	    glm::vec3 kneeToFeetNPose = {0, -0.5, 0};
	
	    return glm::rotate(offset
	                           + getRotated(pelvisToKneeNPose, yawPitchRollUp[PITCH_INDEX], yawPitchRollUp[ROLL_INDEX])
	                           + getRotated(kneeToFeetNPose, yawPitchRollBottom[PITCH_INDEX], yawPitchRollBottom[ROLL_INDEX]),
	                       yaw,
	                       glm::vec3{0, 1, 0});
	}
	
	glm::vec3 getFootPosition(float yaw, const std::vector<float>& offset, const std::vector<float>& yawPitchRollUp, const std::vector<float>& yawPitchRollBottom)
	{
	    return getFootPosition(yaw,
	                           glm::vec3{offset[0], offset[1], offset[2]},
	                           glm::vec3{yawPitchRollUp[0], yawPitchRollUp[1], yawPitchRollUp[2]},
	                           glm::vec3{yawPitchRollBottom[0], yawPitchRollBottom[1], yawPitchRollBottom[2]});
	}
	
	void getTrackersPosition(Message message, glm::vec3& leftFootPosition, glm::vec3& rightFootPosition)
	{
        TrackersPosition TrackersPosition;

	    static biomodel::deepModel::Model model = biomodel::deepModel::Treadmill().getModel();
	    model.update(message);
	
	    float modelYaw = model.get(Part::CRANE)[YAW_INDEX];
	
	    TrackersPosition.leftFootPosition = getFootPosition(modelYaw,
	                                                 model.get(Part::LEFT_LEG_OFFSET),
	                                                 model.get(Part::LEFT_LEG_UPPER),
	                                                 model.get(Part::LEFT_LEG_LOWER));
	
	    TrackersPosition.rightFootPosition = getFootPosition(modelYaw,
	                                                  model.get(Part::RIGHT_LEG_OFFSET),
	                                                  model.get(Part::RIGHT_LEG_UPPER),
	                                                  model.get(Part::RIGHT_LEG_LOWER));
		
		TrackersPosition.leftFootPosition[1] = std::min(TrackersPosition.leftFootPosition[1], 1.0F);
		TrackersPosition.rightFootPosition[1] = std::min(TrackersPosition.rightFootPosition[1], 1.0F);
		
		leftFootPosition = TrackersPosition.leftFootPosition;
        rightFootPosition = TrackersPosition.rightFootPosition;
	}
}

