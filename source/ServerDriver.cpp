// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.


#include "ServerDriver.h"

#define MAX_XBOX_STICK (32767)
#define MAX_XBOX_TRIGGER (255)
const char* ConfigFileName = "drivers\\gamenastix\\driver.vrdrivermanifest";
boost::property_tree::ptree ConfigJsonPropertyTree;
std::string LastLine;
boost::thread SerialReaderThread;
std::int32_t LogLevel;
ServerDriver::InputModes InputMode;

void InitLog()
{
    boost::log::add_file_log(boost::log::keywords::file_name = "trace_%N.log",
                             boost::log::keywords::rotation_size = 10 * 1024 * 1024,
                             boost::log::keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
                             boost::log::keywords::auto_flush = true,
                             boost::log::keywords::format = "[%TimeStamp%]: %Message%");


    boost::log::core::get()->set_filter(
        boost::log::trivial::severity >= boost::log::trivial::trace);
}

void ServerDriver::LoadConfig()
{
    boost::property_tree::read_json(ConfigFileName, ConfigJsonPropertyTree);
    InputMode = GetInputModeEnum(ConfigJsonPropertyTree.get<std::string>("Input", "XBOX"));
    LogLevel = ConfigJsonPropertyTree.get<int32_t>("LogLevel", 0);
    boost::log::core::get()->set_filter(
        boost::log::trivial::severity >= LogLevel);
}

ServerDriver::InputModes ServerDriver::GetInputModeEnum(std::string InputMode)
{
    if (InputMode == "XBOX")
    {
        return ServerDriver::XBOX;
    }
    else if (InputMode == "COM")
    {
        return ServerDriver::COM;
    }
}

vr::EVRInitError ServerDriver::Init(vr::IVRDriverContext* driverContext)
{
    InitLog();

    VR_INIT_SERVER_DRIVER_CONTEXT(driverContext);

    LoadConfig();

    vr::DriverPose_t testPose = {0};
    testPose.deviceIsConnected = true;
    testPose.poseIsValid = true;
    testPose.willDriftInYaw = false;
    testPose.shouldApplyHeadModel = false;
    testPose.poseTimeOffset = 0;
    testPose.result = vr::ETrackingResult::TrackingResult_Running_OK;
    testPose.qDriverFromHeadRotation = {1, 0, 0, 0};
    testPose.qWorldFromDriverRotation = {1, 0, 0, 0};

    vr::VRControllerState_t testState;
    testState.ulButtonPressed = testState.ulButtonTouched = 0;

    trackerFootLeft = DiscoTracker("disco_tracker_foot_left", testPose, testState);
    trackerFootRight = DiscoTracker("disco_tracker_foot_right", testPose, testState);

    vr::VRServerDriverHost()->TrackedDeviceAdded("disco_tracker_foot_left", vr::TrackedDeviceClass_GenericTracker, &trackerFootLeft);
    vr::VRServerDriverHost()->TrackedDeviceAdded("disco_tracker_foot_right", vr::TrackedDeviceClass_GenericTracker, &trackerFootRight);

    boost::unique_lock<boost::shared_mutex> ConfigLock{SerialPort::ConfigMutex};
    SerialPort::ConfigFileName = ConfigFileName;
    ConfigLock.unlock();


    boost::thread SerialReaderThread{SerialPort::SerialPortReader};


    return vr::EVRInitError::VRInitError_None;
}

void ServerDriver::Cleanup()
{
    // TODO
}

const char* const* ServerDriver::GetInterfaceVersions()
{
    return vr::k_InterfaceVersions;
}

namespace {
double scaleShort(short value, double rangeMeters)
{
    return static_cast<double>(value) / 32768.0 * rangeMeters;
}

double scaleByte(BYTE value, double rangeMeters)
{
    return static_cast<double>(value) / 256.0 * rangeMeters;
}
}

void ServerDriver::SetTracker(vr::DriverPose_t& driverPose, DiscoTracker& discoTracker, short padX, short padY, BYTE padZ)
{
    vr::DriverPose_t currentPose = discoTracker.GetPose();

    currentPose.vecPosition[0] = scaleShort(padX, 1.0);
    currentPose.vecPosition[1] = scaleByte(padZ, 0.5) - 1.2;
    currentPose.vecPosition[2] = -scaleShort(padY, 1.0);

    discoTracker.UpdateControllerPose(currentPose);
    vr::VRServerDriverHost()->TrackedDevicePoseUpdated(discoTracker.getObjectID(), discoTracker.GetPose(), sizeof(vr::DriverPose_t));

    driverPose = currentPose;
}

void ServerDriver::RunFrame()
{
    static vr::DriverPose_t leftPose = trackerFootLeft.GetPose();
    static vr::DriverPose_t rightPose = trackerFootRight.GetPose();

    static std::chrono::milliseconds lastMillis = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    std::chrono::milliseconds deltaTime = now - lastMillis;
    lastMillis = now;

    switch (InputMode)
    {
        case ServerDriver::XBOX:
		{
			static auto Gamepad = new XBoxController(1);
			auto xInputGamepad = Gamepad->GetState().Gamepad;
			SetTracker(leftPose, trackerFootLeft, xInputGamepad.sThumbLX, xInputGamepad.sThumbLY, xInputGamepad.bLeftTrigger);
			SetTracker(rightPose, trackerFootRight, xInputGamepad.sThumbRX, xInputGamepad.sThumbRY, xInputGamepad.bRightTrigger);
            break;
		}
        case ServerDriver::COM:
        {
            boost::shared_lock<boost::shared_mutex> LastReadedLineLock{SerialPort::LastReadLineMutex};
            if (LastLine != SerialPort::LastReadedLine)
            {
                LastLine = SerialPort::LastReadedLine;
                BOOST_LOG_TRIVIAL(trace) << "Readed Line: " + SerialPort::LastReadedLine;

                Message message(LastLine.begin(), LastLine.end());
                if (Matcher::match(message) == MessageType::Frame)
                {
                    FeetPosition::getTrackersPosition(message, TrackersPosition.leftFootPosition, TrackersPosition.rightFootPosition);
                }

                BOOST_LOG_TRIVIAL(trace) << "Left Tracker Position: [ X: " + boost::lexical_cast<std::string>(TrackersPosition.leftFootPosition[0] * 100000) + " | Y: " + boost::lexical_cast<std::string>(TrackersPosition.leftFootPosition[1] * 100000) + " | Z: " + boost::lexical_cast<std::string>(TrackersPosition.leftFootPosition[2] * 100000) + " ]";
                BOOST_LOG_TRIVIAL(trace) << "Right Tracker Position: [ X: " + boost::lexical_cast<std::string>(TrackersPosition.rightFootPosition[0] * 100000) + " | Y: " + boost::lexical_cast<std::string>(TrackersPosition.rightFootPosition[1] * 100000) + " | Z: " + boost::lexical_cast<std::string>(TrackersPosition.rightFootPosition[2] * 100000) + " ]";
            }
            SetTracker(leftPose, trackerFootLeft, TrackersPosition.leftFootPosition[0] * 100000, TrackersPosition.leftFootPosition[2] * 100000, TrackersPosition.leftFootPosition[1] * 100000);
            SetTracker(rightPose, trackerFootRight, TrackersPosition.rightFootPosition[0] * 100000, TrackersPosition.rightFootPosition[2] * 100000, TrackersPosition.rightFootPosition[1] * 100000);
            break;
        }
        default:
            break;
    }
}

bool ServerDriver::ShouldBlockStandbyMode()
{
    return false;
}

void ServerDriver::EnterStandby()
{
}

void ServerDriver::LeaveStandby()
{
}

vr::HmdQuaternion_t ServerDriver::ConvertEulerAnglesToQuaternion(double yawDegrees, double pitchDegrees, double rollDegrees)
{
    vr::HmdQuaternion_t quaternion;

    double cosYaw = cos(glm::radians(yawDegrees) * 0.5);
    double sinYaw = sin(glm::radians(yawDegrees) * 0.5);
    double cosRoll = cos(glm::radians(rollDegrees) * 0.5);
    double sinRoll = sin(glm::radians(rollDegrees) * 0.5);
    double cosPitch = cos(glm::radians(pitchDegrees) * 0.5);
    double sinPitch = sin(glm::radians(pitchDegrees) * 0.5);

    quaternion.w = cosYaw * cosRoll * cosPitch + sinYaw * sinRoll * sinPitch;
    quaternion.x = sinYaw * cosRoll * cosPitch - cosYaw * sinRoll * sinPitch;
    quaternion.y = cosYaw * cosRoll * sinPitch + sinYaw * sinRoll * cosPitch;
    quaternion.z = cosYaw * sinRoll * cosPitch - sinYaw * cosRoll * sinPitch;

    return quaternion;
}