// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once
#include <windows.h>
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <XInput.h>

#pragma comment(lib, "XInput.lib")

class XBoxController
{
private:
    XINPUT_STATE xInputState;
    DWORD controllerIndex;

    void ResetInternalState();
    DWORD GetInternalState();

public:
    XBoxController(int playerNumber);
    XINPUT_STATE GetState();
    bool IsConnected();
};
