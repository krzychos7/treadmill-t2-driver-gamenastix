// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once

#include <string>

#include <openvr_driver.h>


class DiscoTracker : public vr::ITrackedDeviceServerDriver
{
public:
    DiscoTracker() = default;

    ~DiscoTracker() = default;

    DiscoTracker(std::string serialName, vr::DriverPose_t initialControllerPose, vr::VRControllerState_t initialControllerState);

    virtual void UpdateControllerState(vr::VRControllerState_t newControllerState);

    virtual void UpdateControllerPose(vr::DriverPose_t newControllerPose);

    virtual uint32_t getObjectID();

    vr::EVRInitError Activate(uint32_t activatedObjectId) override;

    void Deactivate() override;

    void EnterStandby() override;

    void* GetComponent(const char* componentNameAndVersion) override;

    void DebugRequest(const char* requestString, char* responseBuffer, uint32_t responseBufferSize) override;

    vr::DriverPose_t GetPose() override;

    vr::VRControllerState_t GetControllerState();

    bool TriggerHapticPulse(uint32_t unAxisId, uint16_t usPulseDurationMicroseconds);

private:
    vr::VRControllerState_t controllerState;

    vr::DriverPose_t controllerPose;

    uint32_t objectId;

    std::string serialName;
};