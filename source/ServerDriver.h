// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once

#pragma warning(disable : 4996)
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/asio.hpp>
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <SerialPort.h>
#include "DiscoTracker.h"
#include "XBoxController.h"
#include "stdio.h"
#include <openvr_driver.h>
#include <algorithm>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/glm.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "deepModel/Treadmill.h"
#include "FeetPosition.h"
#include <Protocol.h>
#include <windows.h>



class ServerDriver : public vr::IServerTrackedDeviceProvider
{
public:
    ServerDriver() = default;

    ~ServerDriver() = default;

    vr::EVRInitError Init(vr::IVRDriverContext *driverContext) override;

    static vr::HmdQuaternion_t ConvertEulerAnglesToQuaternion(double yawDegrees, double pitchDegrees, double rollDegrees);

    void Cleanup() override;

    const char *const *GetInterfaceVersions() override;

    void RunFrame() override;

    bool ShouldBlockStandbyMode() override;

    void EnterStandby() override;

    void LeaveStandby() override;

	enum InputModes
	{
		XBOX,
		COM
	};

private:
    void ServerDriver::SetTracker(vr::DriverPose_t &driverPose, DiscoTracker &discoTracker, short padX, short padY, BYTE padZ);
    void ServerDriver::LoadConfig();
    ServerDriver::InputModes ServerDriver::GetInputModeEnum(std::string InputMode);

    DiscoTracker trackerFootLeft;
    DiscoTracker trackerFootRight;
    FeetPosition::TrackersPosition TrackersPosition;

	
};
