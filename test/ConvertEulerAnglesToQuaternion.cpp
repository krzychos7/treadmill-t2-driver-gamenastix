#include "DiscoTracker.cpp"
#include "SerialPort.cpp"
#include "ServerDriver.cpp"
#include "XBoxController.cpp"
#include "gtest/gtest.h"
#include <windows.h>

void testConvertYawPitchRollToQuaternion(int yaw, int pitch, int roll, double expectW, double expectX, double expectY, double expectZ)

{
    auto quat = ServerDriver::ConvertEulerAnglesToQuaternion(yaw, pitch, roll);
    auto delta = 0.001;

    EXPECT_NEAR(quat.w, expectW, delta);
    EXPECT_NEAR(quat.x, expectX, delta);
    EXPECT_NEAR(quat.y, expectY, delta);
    EXPECT_NEAR(quat.z, expectZ, delta);
}

TEST(ServerDriverTestSuite, ConvertEulerAnglesToQuaternion_ReferenceAxisOrderTest)
{
    testConvertYawPitchRollToQuaternion(90, 0, 0, 0.707, 0.707, 0, 0);
    testConvertYawPitchRollToQuaternion(0, 90, 0, 0.707, 0, 0.707, 0);
    testConvertYawPitchRollToQuaternion(0, 0, 90, 0.707, 0, 0, 0.707);
    testConvertYawPitchRollToQuaternion(45, 45, 45, 0.844, 0.191, 0.461, 0.191);
}